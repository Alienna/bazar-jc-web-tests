import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

public class CheckProjectTest extends Common {

    @Test
    public void testOpenLoginPage(){
        webDriver.get(baseUrl);
        assertEquals("http://bazar.jettycloud.com/login", webDriver.getCurrentUrl());
    }
}
