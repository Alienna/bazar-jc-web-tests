import config.Config;
import io.restassured.response.Response;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.io.IOException;

import static io.restassured.RestAssured.given;

public abstract class Common {
    WebDriver webDriver;
    String baseUrl;
    @BeforeTest
    public void setUp() {
        //Чтобы вычитать настройки из конфига
        Config config;
        {
            try {
                config = new Config();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        webDriver = new ChromeDriver();
        baseUrl = config.getBaseUrl();
    }
    @AfterTest
    public void tearDown() {
        // Закрываем браузер
        webDriver.quit();
    }
    public static Response getUserTime (String o) {
        return given()
                .pathParam("uid", o)
                .when()
                .get("");
    }
}
